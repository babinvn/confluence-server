# JDK8 Confluence server

Confluence docker image comes with jdk11 by default. If you want JDK8 (as in JIRA docker image), replace
`ARG BASE_IMAGE=adoptopenjdk:11-hotspot` with `ARG BASE_IMAGE=adoptopenjdk:8-hotspot` and rebuild.
For building instructions please refer to: https://bitbucket.org/atlassian-docker/docker-atlassian-confluence-server

```
export CONFLUENCE_VERSION=7.6.2
git clone https://bitbucket.org/atlassian-docker/docker-shared-components.git
git clone https://bitbucket.org/atlassian-docker/docker-atlassian-confluence-server/
cp -r docker-shared-components/image/ docker-atlassian-confluence-server/shared-components/
cp -r docker-shared-components/support/ docker-atlassian-confluence-server/shared-components/
pushd docker-atlassian-confluence-server > /dev/null
sed -i 's/adoptopenjdk:11-hotspot/adoptopenjdk:8-hotspot/g' Dockerfile
docker build --tag "confluence:${CONFLUENCE_VERSION}" --build-arg "CONFLUENCE_VERSION=${CONFLUENCE_VERSION}" .
popd > /dev/null
```
